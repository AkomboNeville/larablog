<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @yield('title')
    @vite('resources/css/app.css')
</head>
<body>
@include('components.header')
<main class="py-6">
    @yield('content')
</main>
@include('components.footer')
</body>
</html>
