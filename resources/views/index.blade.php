@extends('layouts.app')

@section('title')
    <title>Home|What is you story</title>
@endsection

@section('content')
    <section class="hero">
        <div class="container">
            <h1>Hero Section</h1>
        </div>
    </section>

    <section class="search form">
        <div class="container">
            <h1>Search Section</h1>
        </div>
    </section>

    <section class="posts">
        <div class="container">
            <h1>Post Section</h1>
        </div>
    </section>
    <section class="home-cta">
        <div class="container">
            <h1>Post CTA Section</h1>
        </div>
    </section>

@endsection
