<header>
    <div id="site-logo" class="text-center bg-transparent">
        <h1>LARABLOG</h1>
    </div>
    <nav class="flex justify-center items-center">
        <ul class="mr-1 flex justify-between items-center">
            <li class="px-4">
                <a href="">Home</a>
            </li>
            <li class="px-4">
                <a href="">Blogs</a>
            </li>
            <li class="px-4">
                <a href="">About</a>
            </li>
            <li class="px-4">
                <a href="">Contact Us</a>
            </li>
        </ul>
        <ul class="mr-1 flex justify-between items-center">
            @auth
                <li class="px-4">
                    <a href="">{{auth()->user()->first_name}}</a>
                </li>
            @endauth
            @guest
            <li class="px-4">
                <a href="" color="lara-primary-color ">Login</a>
            </li>
            <li class="lara-btn outline">
                <a href="{{route('register')}}">Register</a>
            </li>
            @endguest
        </ul>
    </nav>
</header>
