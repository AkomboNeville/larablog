@extends('layouts.app')

@section('title')
    <title>Register|What is you story</title>
@endsection

@section('content')
    <section class="form-wrapper text-center first-section">
        <div class="container">
            <h2 class="lara-heading">REGISTER</h2>
            <form action="{{route('register')}}" method="post" class="lara-form w-5/12 mx-auto">
                <div class="flex justify-between mb-5 row">
                    <div class="w-6/12 col">
                        <label for="first-name" class="sr-only">First Name</label>
                        <input type="text" name="first_name" id="first-name" placeholder="First name">
                    </div>
                    <div class="w-6/12 col">
                        <label for="last-name" class="sr-only">Last Name</label>
                        <input type="text" name="last_name" id="last-name" placeholder="last name">
                    </div>
                </div>
                <div class="flex justify-between mb-5">
                    <div class="w-full">
                        <label for="email" class="sr-only">Email</label>
                        <input type="text" name="email" id="email" placeholder="Email">
                    </div>
                </div>
                <div class="flex justify-between mb-5">
                    <div class="w-full">
                        <label for="phone" class="sr-only">Phone Number</label>
                        <input type="tel" name="tel" id="phone" placeholder="Phone number">
                    </div>
                </div>
                <div class="flex justify-between mb-5">
                    <div class="w-full">
                        <label for="address" class="sr-only">Address</label>
                        <input type="text" name="address" id="address" placeholder="Address">
                    </div>
                </div>
                <div class="flex justify-between mb-5">
                    <div class="w-full">
                        <label for="password" class="sr-only">Address</label>
                        <input type="text" name="password" id="password" placeholder="Password">
                    </div>
                </div>
                <div class="flex justify-between mb-5">
                    <div class="w-full">
                        <label for="password-confirmation" class="sr-only">Address</label>
                        <input type="text" name="password_confirmation" id="password-confirmation" placeholder="Repeat Password">
                    </div>
                </div>
                <div class="flex justify-between mb-5">
                    <div class="w-full">
                        <button type="submit" class="lara-btn fill w-full rounded">Register</button>
                    </div>
                </div>
            </form>

        </div>
    </section>

@endsection
