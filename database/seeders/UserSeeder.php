<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // get admin role id
        $role = DB::table('roles')->where('name','Admin')->first();
        if ($role){
            DB::table('users')->insert([
                'first_name' => 'Akombo Neville',
                'last_name' => 'Akombo Neville',
                'email' => 'akomboneville55@gmail.com',
                'phone' => '+237672345338',
                'password' => Hash::make('password'),
                'role_id' => $role->id,
                'address' => 'Akombo Neville',
            ]);
        }

    }
}
